import React, {Component} from "react";
import TodoItems from "./TodoItems";

class TodoList extends Component {
    constructor(props) {
        super(props);
        this.state = {
            items: [],
            filterStatus: 'all'
        };
        this.addItem = this.addItem.bind(this);
        this.handleChange=this.handleChange.bind(this);
        this.deleteItem = this.deleteItem.bind(this);
        this.completeItem = this.completeItem.bind(this);
    }

    handleChange(e){
        this.setState({
            content: e.target.value
        });
    }

    addItem(e) {
        if (this._inputElement.value !== "") {
            var newItem = {
                content: this._inputElement.value,
            };

            this.setState((prevState) => {
                return {
                    items: prevState.items.concat(newItem)
                };
            });

            this._inputElement.value = "";
        }

        console.log(this.state.items);

        e.preventDefault();
    }

    deleteItem(id) {
        const index = this.state.items.findIndex(item => item.id === id);
        if (index === -1)
            return;
        this.state.items.splice(index, 1);
        this.setState(this.state);
    }

    completeItem(item) {
        item.complete = !item.complete;
        this.setState(this.state);
    }

    render() {
        let items = this.state.items;
        let filterStatus = this.state.filterStatus;
        const elmItem = items.map((item, index) => {
            return (
                <TodoItems onClickComplete={this.completeItem} onClickDelete={this.deleteItem}
                           onClickSetCompleted={this.handleSetCompleted} filterStatus={filterStatus}
                           key={index} item={item}/>
            );
        });
        return (
            <div className="todoListMain">
                <header className="header">
                    <h1>todos</h1>
                    <form onSubmit={this.addItem}>
                        <section id="main">
                            {/*<input id="toggle-all" type="checkbox" onClick={()=>this.handleGetItem(items)}/>*/}
                            <input id="new-todo" type="text" onChange={this.handleChange} ref={(a) => this._inputElement = a}  placeholder="What needs to be done?">
                            </input>
                            <label htmlFor="toggle-all">Mark all as complete</label>
                        </section>
                    </form>
                </header>
                <div className="main-todo">
                    <ul id="todo-list">
                        {elmItem}
                    </ul>
                </div>
                <footer className="footer">
                    <ul id="filters">
                        <li>
                            <a role="button">All</a>
                        </li>
                        <li>
                            <a role="button">Active</a>
                        </li>
                        <li>
                            <a role="button">Completed</a>
                        </li>
                    </ul>
                    <button id="clear-completed" role="button">Clear completed</button>
                </footer>
            </div>
        );
    }
}

export default TodoList;